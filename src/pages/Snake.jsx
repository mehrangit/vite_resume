import { Component } from "react";
import House from "../components/snake/House";
import Loose from "../components/snake/Loose";
import Game from "../components/snake/Game";

class Snake extends Component {

    constructor(props) {
        super(props);

        this.state = {
            segments: [this.randomCell()],
            prevSegments: null,
            food: this.randomCell(),
            speed: { x: 1, y: 0 },
            direction: "right",
            forbiddenDirection: "left",
            buffered: null,
            looseCount: 0,
            seconds: 0,
            gameOver: false,
            audio: new Audio('../../public/sound/gameOver.wav'),
            timer: null,
        }

        this.playAgain = this.playAgain.bind(this)

    }


    setOnBox(segment) {
        if (segment.column > 19) {
            segment.column = 0
        }
        if (segment.column < 0) {
            segment.column = 19
        }
        if (segment.row > 19) {
            segment.row = 0
        }
        if (segment.row < 0) {
            segment.row = 19
        }
        return segment
    }

    randomCell() {
        return {
            row: Math.floor(Math.random() * 20),
            column: Math.floor(Math.random() * 20)
        }
    }

    // choose food & snake cell randomly
    draw(num) {
        const items = [];

        for (let i = 0; i < num; i++) {
            for (let j = 0; j < num; j++) {
                if (i == this.state.food.row && j == this.state.food.column) {
                    // food
                    items.push(<House classes="bg-[#faae42] transition-color border border-dark-100 dark:border-dark-600 flex-1 aspect-w-1 aspect-h-1 aspect-square duration-300 rounded-full" />)
                } else {
                    items.push(<House classes="bg-violet-700 dark:bg-indigo-400 transition-color border border-dark-100 dark:border-dark-600 flex-1 aspect-w-1 aspect-h-1 aspect-square duration-300 rounded-full" />)
                }
            }
        }

        // choose snake cell on load
        for (let i = 0; i < this.state.segments.length; i++) {
            let segment = this.state.segments[i]
            items[segment.row * 20 + segment.column] = <House classes="bg-[#93E088] transition-color border border-dark-100 dark:border-dark-600 flex-1 aspect-w-1 aspect-h-1 aspect-square duration-300 rounded-full" />
        }

        return items;
    }

    countTime() {
        let seconds = this.state.seconds
        seconds++
        this.setState({
            seconds
        })
    }

    componentDidMount() {
        setInterval(() => {
            this.moveSnake()
        }, 250);

        window.addEventListener("keydown", this.handleArrowKeyDown)

        setInterval(() => {
            this.countTime()
        }, 1000);

    }

    snakeEatsFood() {
        let segment = this.state.segments[this.state.segments.length - 1]
        if (segment.column == this.state.food.column && segment.row == this.state.food.row) {
            return true
        } else {
            return false
        }
    }

    endGame() {
        let snakeHead = this.state.segments[this.state.segments.length - 1]

        for (let i = 0; i < this.state.segments.length - 1; i++) {
            let segment = this.state.segments[i]
            if (snakeHead.column === segment.column && snakeHead.row === segment.row) {

                return true
            }
        }
        return false
    }

    moveSnake() {
        if (this.state.buffered != null) {
            this.setState(function (state, props) {
                if (state.direction !== state.buffered.direction) {
                    state.direction = state.buffered.direction;
                    state.speed = state.buffered.speed;
                    state.forbiddenDirection = state.buffered.forbiddenDirection;
                }
                return null
            })
        }
        let segments = this.state.segments
        let head = segments[segments.length - 1]
        let food = this.state.food
        let looseCount = this.state.looseCount;

        if (this.snakeEatsFood()) {
            segments.push(head)
            food = this.randomCell()
        }
        segments.shift();
        segments.push(this.setOnBox({
            column: head.column + this.state.speed.x,
            row: head.row + this.state.speed.y
        }))
        if (this.endGame()) {
            let time = this.state.seconds
            let h = Math.floor(time / 3600);
            let m = Math.floor(time % 3600 / 60);
            let s = Math.floor(time % 3600 % 60);

            let hDisplay, mDisplay, sDisplay
            let lang = localStorage.getItem("lang")
            
            if (lang == null || lang === "en") {
                hDisplay = h > 0 ? h + (h == 1 ? " Hour, " : " Hours, ") : "";
                mDisplay = m > 0 ? m + (m == 1 ? " Minute, " : " Minutes, ") : "";
                sDisplay = s > 0 ? s + (s == 1 ? " Second" : " Seconds") : "";
            } else if (lang === "fa") {
                hDisplay = h > 0 ? h + (h == 1 ? " ساعت, " : " ساعت, ") : "";
                mDisplay = m > 0 ? m + (m == 1 ? " دقیقه, " : " دقیقه, ") : "";
                sDisplay = s > 0 ? s + (s == 1 ? " ثانیه" : " ثانیه") : "";
            }

            looseCount++
            this.setState({
                looseCount,
                gameOver: true,
                timer: hDisplay + mDisplay + sDisplay,
                prevSegments: segments.length
            })
            segments = [segments[segments.length - 1]]
            food = this.randomCell()
            this.state.audio.play()
        }
        this.setState({
            segments: segments,
            food: food,
        })
    }

    playAgain() {
        this.setState({
            seconds: 0,
            timer: null,
            gameOver: false,
            segments: [this.randomCell()],
            food: this.randomCell(),
        })
    }

    handleArrowKeyDown = (event) => {
        let lang = localStorage.getItem("lang")
        let x, y;
        let direction = this.state.direction
        let forbiddenDirection = this.state.forbiddenDirection

        switch (event.keyCode) {
            case 37:
                direction = "left"
                forbiddenDirection = "right"
                break;
            case 38:
                direction = "up"
                forbiddenDirection = "down"
                break;
            case 39:
                direction = "right"
                forbiddenDirection = "left"
                break;
            case 40:
                direction = "down"
                forbiddenDirection = "up"
                break;

            default:
                break;
        }

        if (lang == null || lang === "en") {
            x = direction === "right" ? 1 : direction === "left" ? -1 : 0;
        } else if (lang === "fa") {
            x = direction === "right" ? -1 : direction === "left" ? 1 : 0;
        }
        y = direction === "down" ? 1 : direction === "up" ? -1 : 0


        if (direction !== this.state.direction && direction !== this.state.forbiddenDirection) {
            this.setState(function (state, props) {
                state.buffered = {
                    direction,
                    forbiddenDirection,
                    speed: { x, y }
                }
                return null
            })
        }
    }



    render() {
        return (
            this.state.gameOver ? (<Loose score={this.state.prevSegments} timer={this.state.timer} playAgain={this.playAgain} />) : (
                <Game score={this.state.segments.length} looseCount={this.state.looseCount} timer={this.state.seconds} squares={this.draw(20)} />
            )
        )

    }
}

export default Snake;