import { Component } from "react";
import { connect } from "react-redux";
import { AddCourseToCart, DeleteItemFromCart } from '../stateManagement/Actions/index'
import Checkout from "../components/cart/Checkout";
import { checkCartList } from "../stateManagement/Store";
import Product from "../components/cart/product";
import { withTranslation } from 'react-i18next';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.fillCartTransition = this.fillCartTransition.bind(this)
        this.removeItemFromCart = this.removeItemFromCart.bind(this)
        this.loadData = this.loadData.bind(this)

    }

    loadData() {
        this.props.loadDataFromStore();
    }

    state = {
        courses: [],
        cartList: checkCartList(),
        categoryFilter: ['Free', 'Premium']
    }

    fillCartTransition(e, id) {

        const course = e.target.offsetParent
        const position = course.getBoundingClientRect()
        const body = document.querySelector("body")
        const floatingCart = document.createElement('div')
        floatingCart.setAttribute('class', `floating-cart flex flex-col transition-all duration-700 origin-top-left rtl:origin-top-right`)

        if(document.dir === "rtl"){
            floatingCart.setAttribute('style', `right: ${document.body.clientWidth - position.left - position.width}px; top: ${position.top}px; left: auto`)
        } else {
            floatingCart.setAttribute('style', `left: ${position.left}px; top: ${position.top}px`)
        }

        const imageBox = document.createElement('div')
        imageBox.setAttribute('class', 'w-full h-64')
        const image = document.createElement('img')
        image.setAttribute('src', e.target.offsetParent.firstChild.firstChild.src)
        image.setAttribute('class', "w-full h-full")
        imageBox.appendChild(image)

        const cardInfo = document.createElement('div')
        cardInfo.setAttribute('class', 'w-full p-4 bg-dark-100 dark:bg-dark-600')
        const name = document.createElement('p')
        name.setAttribute('class', 'mb-2 text-lg font-medium title-color color-transition line-clamp-1')
        name.innerText = e.target.offsetParent.lastChild.firstChild.innerText
        const price = document.createElement('p')
        price.setAttribute('class', 'text-sm font-normal text-green-500 color-transition')
        price.innerText = e.target.offsetParent.lastChild.getElementsByTagName('p')[1].innerText
        cardInfo.appendChild(name)
        cardInfo.appendChild(price)

        floatingCart.appendChild(imageBox)
        floatingCart.appendChild(cardInfo)

        body.appendChild(floatingCart)

        setTimeout(() => {
            if(document.dir === "rtl"){
                floatingCart.classList.add("!right-44", "moveToCart")
            } else {
                floatingCart.classList.add("!left-44", "moveToCart")
            }
        }, 50);
        setTimeout(() => {
            floatingCart.classList.add("hideInCart")
        }, 750);

        const courses = this.state.courses
        const selectedCourse = courses.find(c => c.id == id)

        const { dispatch, cartList } = this.props
        setTimeout(() => {
            this.props.addCourseToCart(selectedCourse)
            this.setState({
                cartList
            })
            localStorage.setItem("cartList", JSON.stringify(cartList))
        }, 1200);
    }

    removeItemFromCart = (item) => {
        let cartList = this.state.cartList
        let deletedItemId = cartList.find(course => course.id == item.id).id
        let remainingItems = cartList.filter(item => item.id !== deletedItemId)
        this.setState({
            cartList: remainingItems
        })
        this.props.deleteItemFromCart(deletedItemId, cartList)
        let cartListStorage = JSON.parse(localStorage.getItem("cartList"))
        let selectedIndex = cartListStorage.findIndex(course => course.id == item.id)
        cartListStorage.splice(selectedIndex, 1)
        localStorage.setItem("cartList", JSON.stringify(cartListStorage))

        setTimeout(() => {
            // remove item from props cart
            let selectedPropsIndex = this.props.cartList.findIndex(cart => cart.id == item.id)
            this.props.cartList.splice(selectedPropsIndex, 1)
            // restore courses quantity from props
            let selectedCourseIndex = this.props.courses.findIndex(course => course.id == item.id)
            this.props.courses[selectedCourseIndex].quantity = 1
        }, 300);
    }

    componentDidMount() {
        this.setState({ courses: this.props.courses });
    }

    checkCategoryFilter = (event) => {
        let activeFilters = this.state.categoryFilter
        if(!activeFilters.includes(event.srcElement.dataset.filter)){
            activeFilters.push(event.srcElement.dataset.filter)
        } else {
            activeFilters.splice(activeFilters.indexOf(event.srcElement.dataset.filter), 1)
        }
        this.setState({
            ...this.state, categoryFilter: activeFilters
        })
    }
    
    render() {
        const { t } = this.props;

        let categoryFilters = document.getElementsByName("categories")
        for (let i = 0; i < categoryFilters.length; i++) {
            categoryFilters[i].addEventListener("click", this.checkCategoryFilter);
        }

        return (
            <section className="relative flex items-start gap-4 mt-4 number-fa">
                {/* sidebar */}
                <div className="flex flex-col flex-none gap-8 w-72">
                    {/* cart list */}
                    <div className="flex flex-col gap-4">
                        <div className="flex items-center justify-between mb-4">
                            <div className="flex items-center gap-4">
                                <p className="flex-none text-base font-normal title-color"> سبد خرید </p>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-10 h-10 stroke-purple-500 dark:stroke-purple-700 color-transition">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                                </svg>
                            </div>

                            <div className="px-2 py-1 text-base font-medium text-green-500 border-2 border-green-500 rounded cursor-pointer"> {t('common:CHECKOUT')} </div>
                        </div>
                        {this.state.cartList.length == 0 ? (<i className="text-dark-400"> {t('cart:CARTISEMPTY')} </i>) : (
                            <ul className="flex flex-col">
                                {this.state.cartList.map((cart, index) => (
                                    <Checkout cart={cart} deleteAction={this.removeItemFromCart} courses={this.state.courses} key={index} cartItems={this.state.cartList} />
                                ))}
                            </ul>
                        )}
                    </div>

                    {/* categories */}
                    <div className="flex flex-col">
                        <p className="mb-4 text-lg font-medium title-color"> {t('common:CATEGORIES')} </p>
                        <label htmlFor="tailwindFree" className="flex items-center gap-4 mb-4 cursor-pointer">
                            <input defaultChecked="true" type="checkbox" data-filter="Free" name="categories" id="tailwindFree" className="hidden peer" />
                            <p className="relative w-4 h-4 border rounded border-gray transition-colors peer-checked:border-green-400 peer-checked:border-2 before:absolute before:content-[''] before:-top-1 before:left-0 before:-rotate-45 before:border-2 before:border-b-green-600 before:border-t-transparent before:border-l-green-600 before:border-r-transparent before:hidden peer-checked:before:block before:w-5 before:h-2"></p>
                            <p className="text-base title-color color-transition"> {t('common:FREE')} </p>
                        </label>
                        <label htmlFor="tailwind" className="flex items-center gap-4 mb-4 cursor-pointer">
                            <input defaultChecked="true" type="checkbox" data-filter="Premium" name="categories" id="tailwind" className="hidden peer" />
                            <p className="relative w-4 h-4 border rounded border-gray transition-colors peer-checked:border-green-400 peer-checked:border-2 before:absolute before:content-[''] before:-top-1 before:left-0 before:-rotate-45 before:border-2 before:border-b-green-600 before:border-t-transparent before:border-l-green-600 before:border-r-transparent before:hidden peer-checked:before:block before:w-5 before:h-2"></p>
                            <p className="text-base title-color color-transition"> {t('common:PREMIUM')} </p>
                        </label>
                    </div>
                </div>
                {/* courses */}
                <div className="flex flex-wrap items-center flex-1 gap-4">
                    {/* course */}
                    {this.state.courses.map((course, index) => (
                        <Product filter={this.state.categoryFilter.includes(course.filter)} course={course} key={index} fillCartTransitionAction={this.fillCartTransition} />
                    ))}
                </div>
            </section>
        );
    }
}


let cart = checkCartList()
function mapStateToProps(state) {
    let exist;
    if (state.cartList.action != undefined) {
        exist = cart.find(course => course.id == state.cartList.action.id)
        // if item doesnt exist in cart
        if (!cart.includes(exist) && state.cartList.type !== "DELETE_ITEM") {
            cart.push(state.cartList.action)
            return {
                courses: state.courses,
                cartList: cart
            };
            // if item exist in cart
        } 
        else if (cart.findIndex(course => course.id == state.cartList.action.id) > -1 && state.cartList.type !== "DELETE_ITEM") {
            let copyOfCart = cart
            let selected = copyOfCart.find(course => course.id == state.cartList.action.id)
            let selectedIndex = copyOfCart.findIndex(course => course.id == state.cartList.action.id)
            selected.quantity += 1
            copyOfCart[selectedIndex] = selected
            cart = copyOfCart
            return {
                courses: state.courses,
                cartList: cart
            };
        }
    } else {
        return {
            courses: state.courses,
            cartList: cart
        };
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addCourseToCart: (selectedCourse) => {
            dispatch(AddCourseToCart(selectedCourse))
        },
        deleteItemFromCart: (deletedId, cartList) => {
            let remainingItems = cartList.filter(item => item.id !== deletedId)
            dispatch(DeleteItemFromCart(remainingItems));
        },
    };
};

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Cart));