// import { useEffect } from "react";
// import { useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Experiences from "../components/developer/Experiences";
import Extracurricular from "../components/developer/Extracurricular";
import ShortDescription from "../components/developer/ShortDescription";
import Skills from "../components/developer/Skills";
import Contact from "../components/developer/Contact";

const Developer = () => {

    const { t } = useTranslation();

    const detail = [
        {
            avatar: {
                src: "/img/profiles/mehran/mehran.jpg",
                contact: "/img/profiles/mehran/2.jpg",
                flag: "/img/flags/iran.png",
                country: "Iran"
            },
            name: t("mehranmahmoudi:NAME"),
            short_description: t('mehranmahmoudi:SHORT_DESCRIPTION'),
            social_media: {
                linkedin: "https://www.linkedin.com/in/mestermim/",
                instagram: "https://www.instagram.com/themestermim/",
                gmail: "mailto:Themestermim@gmail.com",
                phone: "tel:00989307065673",
            },
            projects: "7",
            about: t('mehranmahmoudi:ABOUT'),
            experiences: [
                {
                    src: "/img/expriences/mehran/limooshirin.png",
                    name: "mehranmahmoudi:EXPERIENCES:0:NAME",
                    href: "https://www.limooshirin.digital/",
                    time: "mehranmahmoudi:EXPERIENCES:0:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:0:RESPONSIBILITY",
                    description: "mehranmahmoudi:EXPERIENCES:0:DESCRIPTION"
                },
                {
                    src: "/img/expriences/mehran/shahrfarsh.svg",
                    name: "mehranmahmoudi:EXPERIENCES:1:NAME",
                    href: "https://shahrfarsh.com/",
                    time: "mehranmahmoudi:EXPERIENCES:1:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:1:RESPONSIBILITY",
                    description: "mehranmahmoudi:EXPERIENCES:1:DESCRIPTION"
                },
                {
                    src: "/img/expriences/mehran/shahrkhanegi.svg",
                    name: "mehranmahmoudi:EXPERIENCES:2:NAME",
                    href: "https://www.shahrkhanegi.com/",
                    time: "mehranmahmoudi:EXPERIENCES:2:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:2:RESPONSIBILITY",
                    description: "mehranmahmoudi:EXPERIENCES:2:DESCRIPTION"
                },
                {
                    src: "/img/expriences/mehran/helsa.png",
                    name: "mehranmahmoudi:EXPERIENCES:3:NAME",
                    href: "https://shop.novinarka.net/",
                    time: "mehranmahmoudi:EXPERIENCES:3:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:3:RESPONSIBILITY",
                    description: "mehranmahmoudi:EXPERIENCES:3:DESCRIPTION"
                },
                {
                    src: "/img/expriences/mehran/panel.svg",
                    name: "mehranmahmoudi:EXPERIENCES:4:NAME",
                    href: "https://shop.novinarka.net/panel",
                    time: "mehranmahmoudi:EXPERIENCES:4:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:4:RESPONSIBILITY",
                },
                {
                    src: "/img/expriences/mehran/helsa.png",
                    name: "mehranmahmoudi:EXPERIENCES:5:NAME",
                    href: "https://shop.novinarka.net/login",
                    time: "mehranmahmoudi:EXPERIENCES:5:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:5:RESPONSIBILITY",
                },
                {
                    src: "/img/expriences/mehran/iranheadphone.png",
                    name: "mehranmahmoudi:EXPERIENCES:6:NAME",
                    href: "https://iranheadphone.com/",
                    time: "mehranmahmoudi:EXPERIENCES:6:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:6:RESPONSIBILITY",
                },
                {
                    src: "/img/expriences/mehran/twitter.gif",
                    name: "mehranmahmoudi:EXPERIENCES:7:NAME",
                    href: "https://gitlab.com/mehrangit/twitter",
                    time: "mehranmahmoudi:EXPERIENCES:7:TIME",
                    task: "mehranmahmoudi:EXPERIENCES:7:RESPONSIBILITY",
                    description: "mehranmahmoudi:EXPERIENCES:7:DESCRIPTION"
                },
            ],
            skill: [
                {
                    src: "/img/languages/html5.png",
                    name: "HTML",
                    time: "mehranmahmoudi:SKILL:0:TIME",
                },
                {
                    src: "/img/languages/css.png",
                    name: "CSS",
                    time: "mehranmahmoudi:SKILL:1:TIME",
                },
                {
                    src: "/img/languages/tailwind.png",
                    name: "TAILWIND CSS",
                    time: "mehranmahmoudi:SKILL:2:TIME",
                },
                {
                    src: "/img/languages/javascript.png",
                    name: "JAVASCRIPT",
                    time: "mehranmahmoudi:SKILL:3:TIME",
                },
                {
                    src: "/img/languages/vue.png",
                    name: "VUE JS",
                    time: "mehranmahmoudi:SKILL:4:TIME",
                },
                {
                    src: "/img/languages/react.png",
                    name: "REACT JS",
                    time: "mehranmahmoudi:SKILL:5:TIME",
                },
                {
                    src: "/img/languages/alpine.png",
                    name: "َALPINE JS",
                    time: "mehranmahmoudi:SKILL:6:TIME",
                },
                {
                    src: "/img/languages/laravel.png",
                    name: "َLARAVEL BLADE",
                    time: "mehranmahmoudi:SKILL:7:TIME",
                },
            ],
            courses: [
                {
                    src: "/img/activities/mehran/elementary.png",
                    name: "mehranmahmoudi:COURSES:0:NAME",
                    href: "https://toplearn.com/courses/5742/%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D9%85%D9%82%D8%AF%D9%85%D8%A7%D8%AA%DB%8C-tailwind-css",
                },
                {
                    src: "/img/activities/mehran/advance.png",
                    name: "mehranmahmoudi:COURSES:1:NAME",
                    href: "https://toplearn.com/courses/5796/%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D9%BE%DB%8C%D8%B4%D8%B1%D9%81%D8%AA%D9%87-tailwind-css-%D8%A8%D9%87-%D9%87%D9%85%D8%B1%D8%A7%D9%87-alpinejs",
                },
            ]
        }
    ]

    function clciked(id) {

        const my_element = document.getElementById(id);

        my_element.scrollIntoView({
            behavior: "smooth",
            block: "start",
            inline: "nearest"
        });
    }

    return (
        <section className="flex flex-col gap-4 sm:gap-8">
            {/* nav */}
            <ul className="flex items-center gap-4 p-4 overflow-auto text-sm font-medium sm:gap-6 sm:text-base color-transition text-dark-900 dark:text-dark-300">
                <li onClick={() => clciked("about_me")} className="underline cursor-pointer min-w-max decoration-dark-900 dark:decoration-dark-300 decoration-2 underline-offset-8 hover:decoration-purple-500 dark:hover:decoration-purple-700 color-transition"> {t('common:ABOUT_ME')} </li>
                <li onClick={() => clciked("worksamples")} className="underline cursor-pointer min-w-max decoration-dark-900 dark:decoration-dark-300 decoration-2 underline-offset-8 hover:decoration-purple-500 dark:hover:decoration-purple-700 color-transition"> {t('common:EXPRIENCES')} </li>
                <li onClick={() => clciked("skills")} className="underline cursor-pointer min-w-max decoration-dark-900 dark:decoration-dark-300 decoration-2 underline-offset-8 hover:decoration-purple-500 dark:hover:decoration-purple-700 color-transition"> {t('common:SKILLS')} </li>
                <li onClick={() => clciked("activities")} className="underline cursor-pointer min-w-max decoration-dark-900 dark:decoration-dark-300 decoration-2 underline-offset-8 hover:decoration-purple-500 dark:hover:decoration-purple-700 color-transition"> {t('common:EXTRACURRICULAR')} </li>
                <li onClick={() => clciked("contact_us")} className="underline cursor-pointer min-w-max decoration-dark-900 dark:decoration-dark-300 decoration-2 underline-offset-8 hover:decoration-purple-500 dark:hover:decoration-purple-700 color-transition"> {t('common:CONTACT')} </li>
            </ul>
            {/* first col */}
            <section className="flex flex-col py-4">
                <ShortDescription data={detail[0]} />
            </section>
            {/* second col */}
            <section className="flex flex-col">
                <div className="px-6 bg-white border divide-y rounded-lg divide-dark-200 dark:divide-dark-500 color-transition dark:border-dark-500 dark:bg-dark-700 sm:rounded-2xl border-dark-200">
                    {/* description */}
                    <div className="py-6" id="about_me">
                        <p className="mb-4 text-sm font-medium sm:text-base text-dark-500 dark:text-dark-300 color-transition"> {t('common:ABOUT_ME')} </p>
                        <p className="text-sm font-normal select-none text-dark-400 dark:text-dark-300 color-transition sm:leading-6">
                            {detail[0].about}
                        </p>
                    </div>
                    {/* expriences */}
                    <Experiences data={detail[0]} />
                    {/* skills */}
                    <Skills data={detail[0]} />
                    {/* activities */}
                    {detail[0].courses.length != 0 ? (
                        <Extracurricular data={detail[0]} />
                    ) : ""}
                    {/* contact me */}
                    <Contact data={detail[0]} />
                </div>
            </section>
        </section>
    );
}

export default Developer;