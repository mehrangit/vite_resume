import { Link } from "react-router-dom";
import { useTranslation } from 'react-i18next';

const Home = () => {

    const developer = [
        {
            id: 1,
            name: "Mehran_Mahmoudi",
            desc: {
                job: "JOB_FRONT",
                url: "/img/profiles/mehran/mehran.jpg",
                name: "JOB_APPLIER",
                years: "JOB_EXPRIENCE",
                services: [
                    {
                        id: 1,
                        url: "/img/languages/html5.png",
                        alt: "Html"
                    },
                    {
                        id: 2,
                        url: "/img/languages/css.png",
                        alt: "Css"
                    },
                    {
                        id: 3,
                        url: "/img/languages/tailwind.png",
                        alt: "Tailwind"
                    },
                    {
                        id: 4,
                        url: "/img/languages/javascript.png",
                        alt: "Javascript"
                    },
                    {
                        id: 5,
                        url: "/img/languages/react.png",
                        alt: "React"
                    },
                    {
                        id: 6,
                        url: "/img/languages/vue.png",
                        alt: "Vue"
                    },
                    {
                        id: 7,
                        url: "/img/languages/alpine.png",
                        alt: "Alpine"
                    },
                ]
            },
            // detail: {
            //     avatar: {
            //         src: "/img/profiles/mehran/mehran.jpg",
            //         contact: "/img/profiles/mehran/2.jpg",
            //         flag: "/img/flags/iran.png",
            //         country: "Iran"
            //     },
            //     name: "مهران محمودی",
            //     short_description: "سلام به همه، من فرانت اند دولاپر از کرج هستم و دنبال موقعیت کاری جدیدی تو این حوزه هستم.",
            //     social_media: {
            //         linkedin: "https://www.linkedin.com/in/mestermim/",
            //         instagram: "https://www.instagram.com/themestermim/",
            //         gmail: "mailto:Themestermim@gmail.com",
            //         phone: "tel:00989307065673",
            //     },
            //     projects: "7",
            //     about: "آینده رو نمیتونم پیش بینی کنم پس اهداف بلند مدت نمیذارم. هدف های کوچیک تعیین میکنم و به همشون میرسم. تمام تخصص هایم را از روی داکیومنت یاد گرفتم. از هر کسی یه چیزی دوست دارم یاد بگیرم و هر ماه بهتر از ماه قبل باشم. تجربه پروژه های شخصی و کار گروهی رو دارم و بلدم تو کار تیمی چجوری رفتار کنم. در حال حاضر جزو مدرسان سایت تاپلرن هستم. در حال حاضر به صورت پاره وقت قادر به همکاری هستم.",
            //     experiences: [
            //         {
            //             src: "/img/expriences/mehran/limooshirin.png",
            //             name: "لیمو شیرین",
            //             href: "https://www.limooshirin.digital/",
            //             time: "2 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/shahrfarsh.svg",
            //             name: "شهر فرش",
            //             href: "https://shahrfarsh.com/",
            //             time: "1 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/shahrkhanegi.svg",
            //             name: "شهر لوازم خانگی",
            //             href: "https://www.shahrkhanegi.com/",
            //             time: "2 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/helsa.png",
            //             name: "هلسا",
            //             href: "https://shop.novinarka.net/",
            //             time: "4 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/panel.svg",
            //             name: "پنل ادمین",
            //             href: "https://shop.novinarka.net/panel",
            //             time: "14 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/helsa.png",
            //             name: "پنل فروشنده هلسا",
            //             href: "https://shop.novinarka.net/login",
            //             time: "3 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/iranheadphone.png",
            //             name: "ایران هدفون",
            //             href: "https://iranheadphone.com/",
            //             time: "2 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //         {
            //             src: "/img/expriences/mehran/twitter.gif",
            //             name: "توییتر",
            //             href: "https://gitlab.com/mehrangit/twitter",
            //             time: "1 ماه",
            //             task: "توسعه دهنده فرانت",
            //         },
            //     ],
            //     skill: [
            //         {
            //             src: "/img/languages/html5.png",
            //             name: "HTML",
            //             time: "+2 سال",
            //         },
            //         {
            //             src: "/img/languages/css.png",
            //             name: "CSS",
            //             time: "+2 سال",
            //         },
            //         {
            //             src: "/img/languages/tailwind.png",
            //             name: "TAILWIND CSS",
            //             time: "+2 سال",
            //         },
            //         {
            //             src: "/img/languages/javascript.png",
            //             name: "JAVASCRIPT",
            //             time: "+2 سال",
            //         },
            //         {
            //             src: "/img/languages/vue.png",
            //             name: "VUE JS",
            //             time: "6 ماه",
            //         },
            //         {
            //             src: "/img/languages/react.png",
            //             name: "REACT JS",
            //             time: "1 سال",
            //         },
            //         {
            //             src: "/img/languages/alpine.png",
            //             name: "َALPINE JS",
            //             time: "+2 سال",
            //         },
            //         {
            //             src: "/img/languages/laravel.png",
            //             name: "َLARAVEL BLADE",
            //             time: "+2 سال",
            //         },
            //     ],
            //     courses: [
            //         {
            //             src: "/img/activities/mehran/elementary.png",
            //             name: "آموزش مقدماتی TailwindCSS",
            //             href: "https://toplearn.com/courses/5742/%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D9%85%D9%82%D8%AF%D9%85%D8%A7%D8%AA%DB%8C-tailwind-css",
            //         },
            //         {
            //             src: "/img/activities/mehran/advance.png",
            //             name: "آموزش پیشرفته TailwindCSS به همراه Alpinejs",
            //             href: "https://toplearn.com/courses/5796/%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D9%BE%DB%8C%D8%B4%D8%B1%D9%81%D8%AA%D9%87-tailwind-css-%D8%A8%D9%87-%D9%87%D9%85%D8%B1%D8%A7%D9%87-alpinejs",
            //         },
            //     ]
            // }
        },
        // {
        //     id: 2,
        //     name: "roya",
        //     desc: {
        //         job: `JOB_BACK`,
        //         url: "/img/profiles/roya/roya.png",
        //         name: "رویا میرزایی",
        //         years: "+ 2 سال",
        //         services: [
        //             {
        //                 id: 1,
        //                 url: "/img/languages/laravel.png",
        //                 alt: "Laravel"
        //             },
        //         ]
        //     },
        //     detail: {
        //         avatar: {
        //             src: "/img/profiles/roya/roya.png",
        //             contact: "/img/profiles/roya/2.png",
        //             flag: "/img/flags/iran.png",
        //             country: "Iran"
        //         },
        //         name: "رویا میرزایی",
        //         short_description: "سلام به همه، من بک اند دولاپر از کرج هستم و دنبال موقعیت کاری جدیدی تو این حوزه هستم.",
        //         social_media: {
        //             linkedin: "https://www.linkedin.com/in/roya-mirzaei-66204a201/",
        //             instagram: "",
        //             gmail: "",
        //             phone: "",
        //         },
        //         projects: "4",
        //         about: "Experienced Back End Developer with a demonstrated history of working in the computer software industry. Skilled in Management, Leadership, Engineering, Research, and Web Development. Strong engineering professional graduated from Gorgan University of Agriculture and Natural Resources.",
        //         experiences: [
        //             {
        //                 src: "/img/expriences/roya/arka.jpg",
        //                 name: "نوین آرکا",
        //                 href: "https://www.novinarka.net/",
        //                 time: "13 ماه",
        //                 task: "توسعه دهنده بک اند",
        //             },
        //             {
        //                 src: "/img/expriences/roya/negarine.jpg",
        //                 name: "Negarine Group",
        //                 href: "https://negarine.com/",
        //                 time: "13 ماه",
        //                 task: "توسعه دهنده بک اند",
        //             },
        //         ],
        //         skill: [
        //             {
        //                 src: "/img/languages/html5.png",
        //                 name: "HTML",
        //                 time: "+2 سال",
        //             },
        //             {
        //                 src: "/img/languages/css.png",
        //                 name: "CSS",
        //                 time: "+2 سال",
        //             },
        //             {
        //                 src: "/img/languages/laravel.png",
        //                 name: "َLARAVEL",
        //                 time: "+3 سال",
        //             },
        //         ],
        //         courses: []
        //     }
        // },
    ]

    const { t } = useTranslation();

    return (
        <section className="grid grid-cols-4 gap-8 md:grid-cols-8 md:gap-6">
            {developer.map((dev, index) => (
                <Link key={index} to={dev.name} style={{backgroundImage: `url(${dev.desc.url})`}} className={`relative w-full col-span-4 bg-center bg-cover cursor-pointer before:content-[''] before:absolute before:backdrop-brightness-[0.3] before:top-0 before:left-0 before:w-full before:h-full  aspect-4/3 rounded-4xl md:rounded-2xl overflow-hidden border border-transparent lg:rounded-3xl xl:rounded-4xl dark:border-dark-500`}>
                    {/* job */}
                    <p className="absolute text-lg font-medium text-gray-300 whitespace-pre-line sm:text-xl md:text-base top-4 left-4 rtl:left-auto rtl:right-4 sm:top-6 sm:left-6 rtl:sm:left-auto rtl:sm:right-6 md:top-4 md:left-4 rtl:md:left-auto rtl:md:right-4 lg:top-6 lg:left-6 rtl:lg:left-auto rtl:lg:right-6 lg:text-xl"> {t(dev.desc.job)} </p>
                    {/* experience year */}
                    <p className="absolute text-lg font-medium text-gray-300 sm:text-xl md:text-base top-4 right-4 rtl:right-auto rtl:left-4 sm:top-6 sm:right-6 rtl:sm:right-auto rtl:sm:left-6 md:top-4 md:right-4 rtl:md:right-auto rtl:md:left-4 lg:top-6 lg:right-6 rtl:lg:right-auto rtl:lg:left-6 lg:text-xl rtl:number-fa"> {t(dev.desc.years)} </p>
                    {/* developer image & name */}
                    <div className="absolute left-0 z-10 flex flex-col items-center justify-center w-full gap-2 xs:top-12 sm:top-1/2 sm:mt-[-78px] md:top-4 md:mt-0 top-4 lg:top-1/2 lg:mt-[-96px] xl:mt-[-104px]">
                        <div className="overflow-hidden rounded-full w-28 h-28 md:w-24 md:h-24 lg:w-32 lg:h-32 xl:w-36 xl:h-36">
                            <img src={dev.desc.url} alt={dev.desc.name} className="w-full h-full" />
                        </div>
                        <p className="text-2xl font-medium text-center md:text-lg text-zinc-300 lg:text-2xl"> {t(dev.desc.name)} </p>
                    </div>
                    {/* services */}
                    <div className="absolute flex items-center gap-4 overflow-auto scrollbar_hide bottom-4 inset-x-4 sm:bottom-6 sm:inset-x-6 sm:gap-6 md:bottom-4 md:inset-x-4 lg:bottom-6 lg:inset-x-6 md:gap-4">
                        {dev.desc.services.map(service => (
                            <div key={service.id} className="overflow-hidden rounded-full w-14 h-14 sm:w-[72px] sm:h-[72px] md:w-16 md:h-16 xl:w-[72px] xl:h-[72px] flex-center min-w-max">
                                <img src={service.url} alt={service.alt} title={service.alt} className="w-12 h-12 sm:w-16 sm:h-16 md:w-14 md:h-14 xl:w-16 xl:h-16" />
                            </div>
                        ))}
                    </div>
                </Link>
            ))}
        </section>
    );
}

export default Home;