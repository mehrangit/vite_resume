import { useEffect, useState } from "react";
import Darkmode from "./components/navbar/Darkmode";
import Direction from "./components/navbar/Direction";
import Main from "./layout/Main";
import Navbar from "./layout/Navbar";
// import { Link } from "react-router-dom";
import Sidebar from "./layout/Sidebar";


function App() {

    const [scroll, setScroll] = useState(false)
    const [option, setOption] = useState(false)

    window.onscroll = () => {
        if (window.pageYOffset >= 100) {
            if (!scroll) {
                setScroll(true)
            }
        } else {
            if (scroll) {
                setScroll(false)
                setTimeout(() => {
                    setOption(false)
                }, 50);
            }
        }
    }

    console.log(window.location.href);

    useEffect(() => {
        document.body.classList.add('scrollbar-thin', 'scrollbar-thumb-rounded-xl', 'scrollbar-track-[#54545b]', 'scrollbar-thumb-[#323236]', 'hover:scrollbar-thumb-[#1e1e21]', 'custom-scrollbar', 'relative');

    }, [])

    return (
        <div>
            {/* navbar on scroll */}
            <div className={`fixed z-10 transition-all duration-500 rounded-full ${scroll ? "top-6 right-6 rtl:right-auto rtl:left-6" : "-top-32 right-6 rtl:right-auto rtl:left-6"} `}>
                <div className={`absolute top-0 right-0 w-12 h-12 ${option ? "scale-[5]" : ""} transition-all duration-500 bg-purple-800 dark:bg-dark-800 rounded-full -z-10`}></div>
                <div className="w-12 h-12 bg-purple-500 color-transition dark:bg-dark-700 rounded-full shadow-lg cursor-pointer flex-center z-[2]" onClick={() => setOption(!option)}>
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6 stroke-white" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4" />
                    </svg>
                </div>
                {/* darkmode */}
                <div className={`absolute -z-[1] p-2 rounded-full bg-purple-900 dark:bg-dark-900 transition-all duration-500 w-[44px] h-[44px] flex-none ${option ? "-top-2 -left-20 rtl:left-auto rtl:-right-20 scale-100" : "top-0 scale-0 left-0 rtl:left-auto rtl:right-0"}`}>
                    <Darkmode />
                </div>
                {/* direction */}
                <div className={`absolute -z-[1] p-2 rounded-full bg-purple-900 dark:bg-dark-900 transition-all duration-500 w-[44px] h-[44px] flex-none ${option ? "top-20 left-0 scale-100" : "top-0 scale-0 left-0"}`}>
                    <Direction />
                </div>
            </div>

            <Navbar />

            <Sidebar />

            <Main />
        </div>
    );
}

export default App;
