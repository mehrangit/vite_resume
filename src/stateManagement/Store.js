
function checkDarkMode() {
    if (localStorage.getItem('isDark') === null) {
        return false
    } else if (localStorage.getItem('isDark') === 'true') {
        document.body.classList.add('dark')
        return true;
    } else if (localStorage.getItem('isDark') === 'false') {
        document.body.classList.remove('dark')
        return false;
    }
}

function checklang() {
    if (localStorage.getItem('lang') === null) {
        document.dir = "ltr";
        return false;
    } else if (localStorage.getItem('lang') === 'fa') {
        document.dir = "rtl";
        return true;
    } else if (localStorage.getItem('lang') === 'en') {
        document.dir = "ltr";
        return false;
    }
}

export function checkCartList(){
    console.log("store in running");
    if(localStorage.getItem("cartList") != null || localStorage.getItem("cartList") != undefined){
        return JSON.parse(localStorage.getItem("cartList"))
    } else {
        return []
    }
}

export let initializedState = {
    darkmode: checkDarkMode(),
    direction: checklang(),
    total: 0,
    courses: [
        {
            id: 1,
            quantity: 1,
            name: "courses:COURSES:0:NAME",
            src: "../../public/img/activities/mehran/elementary.png",
            price: 0,
            unit: "courses:COURSES:0:UNIT",
            pre: "HTML, CSS",
            filter: "Free"
        },
        {
            id: 2,
            quantity: 1,
            name: "courses:COURSES:1:NAME",
            src: "../../public/img/activities/mehran/advance.png",
            price: 200000,
            unit: "courses:COURSES:1:UNIT",
            pre: "HTML, CSS, JAVASCRIPT",
            filter: "Premium"
        }
    ],
    cartList: checkCartList(),
}