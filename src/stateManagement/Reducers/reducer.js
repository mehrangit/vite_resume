import {initializedState} from "../Store"

export default (state = initializedState, action) => {
    switch (action.type) {
        case "TOGGLE_STATE":
            return {...state, darkmode: !state.darkmode }
        case "FA_DIRECTION":
            return {...state, direction: true }
        case "EN_DIRECTION":
            return {...state, direction: false }
        case "ADD_TO_CART":
            return {...state, cartList: action }
        case "DELETE_ITEM":
            return {...state, cartList: action.action }
        case "READ_ALL":
            return state
        default:
            return state
    }
}