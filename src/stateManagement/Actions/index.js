export const ToggleState = () => {
    return {
        type: "TOGGLE_STATE",
    }
}

export const FaDirection = () => {
    return {
        type: "FA_DIRECTION",
    }
}

export const EnDirection = () => {
    return {
        type: "EN_DIRECTION",
    }
}

export const AddCourseToCart = (payload) => {
    
    return {
        type: "ADD_TO_CART",
        action: payload
    }
}

export const DeleteItemFromCart = (payload) => {
    return {
        type: "DELETE_ITEM",
        action: payload
    }
}

export const ReadCartFromStore = () => {
    return {
        type: "READ_ALL",
    }
}
