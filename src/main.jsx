// import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { BrowserRouter as Router } from 'react-router-dom';
import reducer from './stateManagement/Reducers/reducer';
import { createStore, applyMiddleware } from 'redux';
import thunk from "redux-thunk"; 
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import './i18n/i18n'
import './index.css'

let store = createStore(reducer, applyMiddleware(thunk))

ReactDOM.createRoot(document.getElementById('root')).render(
  <Router>
    <Provider store={store}>
      <App />
      <ToastContainer position="top-right"
        autoClose={2000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl = {localStorage.getItem("lang") === "fa" ? true : false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark" />
    </Provider>
  </Router>,
)
