import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

const Sidebar = () => {
    const { t } = useTranslation();
    return (
        <aside className="fixed z-[1] top-0 bottom-0 left-0 flex flex-col items-center w-16 gap-1 pt-20 transition-colors bg-white rtl:left-auto rtl:right-0 dark:bg-dark-900">
            <Link to="/" className="relative w-16 h-16 flex-center" data-hint={t('MENU_HOME')}>
                <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 stroke-purple-500 dark:stroke-purple-700 color-transition" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                </svg>
            </Link>
            <Link to="/Snake_Game" className="relative w-16 h-16 flex-center" data-hint={t('MENU_SNAKE_GAME')}>
                <svg className="h-7 w-7 fill-purple-500 dark:fill-purple-700 color-transition" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <path d="M22.053 12.181c-4.323-0.675-7.448-1.46-8.965-3.863-1.112-1.761 2.039-5.358 6.095-3.724 0.586 0.983 0.864 2.22 0.957 4.084l5.211 2.468 0.201-0.339 0.004 0.006c0.533-0.28 0.932-0.914 1.304-1.624-0.681 0.291-1.439 0.279-2.162 0.092-0.174-0.114-0.344-0.238-0.506-0.374-1.46-1.222-1.793-2.87-0.745-3.681 0.776-0.6 2.092-0.573 3.315-0.022 0.537 1.128 0.604 2.2 0.697 3.275 0.638-0.874 0.98-1.836 1.159-2.846l0.67-1.13c-3.453-2.642-8.257-3.489-12.044-3.182h-0c0 0 0 0 0 0s-0 0-0 0v0c-12.525 0-11.525 12.323 0 14.103s8.578 11.911 0.618 11.911c-5.252 0-8.788-1.241-11.295-4.003-2.625-2.892-0.779-6.213 2.764-3.24 3.558 2.987 6.258 5.213 10.719 1.746-5.587 1.853-7.776-2.665-11.75-5.108-3.768-2.316-8.613 2.416-5.366 7.565 2.708 4.294 9.53 6.726 15.185 6.726 13.768 0 16.18-16.929 3.936-18.84z"></path>
                </svg>
            </Link>
            <Link to="/Cart" className="relative w-16 h-16 flex-center" data-hint={t('MENU_CART')}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="h-7 w-7 fill-purple-500 dark:fill-purple-700 stroke-purple-500 dark:stroke-purple-700 color-transition">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                </svg>
            </Link>
        </aside>
    );
}

export default Sidebar;