// import { Link } from "react-router-dom";
import Darkmode from "../components/navbar/Darkmode";
import Direction from "../components/navbar/Direction";

const Navbar = () => {

    return (
        <nav className="transition-colors bg-white dark:bg-dark-900">
            <section className="container flex items-center w-full h-16 px-16 rtl:text-rtl">
                <div className="flex items-center flex-1 gap-8">
                    {/* hamburger menu */}
                    {/* <div className="block cursor-pointer">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 stroke-purple-500 dark:stroke-purple-700 color-transition" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </div> */}
                    {/* darkmode */}
                    <Darkmode />
                </div>
                {/* home */}
                <div className="flex justify-center flex-1">
                    {/* <Link to="/"> */}
                    {/* <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 stroke-purple-500 dark:stroke-purple-700 color-transition" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                        </svg> */}
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="h-7 w-7 stroke-purple-500 dark:stroke-purple-700 color-transition">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M15.362 5.214A8.252 8.252 0 0112 21 8.25 8.25 0 016.038 7.048 8.287 8.287 0 009 9.6a8.983 8.983 0 013.361-6.867 8.21 8.21 0 003 2.48z" />
                        <path strokeLinecap="round" strokeLinejoin="round" d="M12 18a3.75 3.75 0 00.495-7.467 5.99 5.99 0 00-1.925 3.546 5.974 5.974 0 01-2.133-1A3.75 3.75 0 0012 18z" />
                    </svg>

                    {/* </Link> */}
                </div>
                {/* direction */}
                <div className="flex justify-end flex-1">
                    <Direction />
                </div>
            </section>
        </nav>
    );
}

export default Navbar;