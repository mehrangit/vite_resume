// import * as React from "react";

import Home from "../pages/Home";
import { Route, Routes } from 'react-router-dom';
import Developer from "../pages/Developer";
import Snake from "../pages/Snake";
import Cart from "../pages/Cart";


const Main = () => {
    return (
        <main className="px-16 py-8 transition-colors min-h-16 dark:bg-dark-600 bg-dark-100">
            <div className="px-8">
                <Routes>
                    <Route path="/" element={<Home />}></Route>
                    <Route path="/:name" element={<Developer />}></Route>
                    <Route path="/Snake_Game" element={<Snake />}></Route>
                    <Route path="/Cart" element={<Cart />}></Route>
                </Routes>
            </div>
        </main>
    );
}

export default Main;