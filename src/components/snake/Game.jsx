import { useTranslation } from "react-i18next";

const Game = ({score, looseCount, timer, squares}) => {
    const {t} = useTranslation();
    return (
        <section className="grid grid-cols-5 h-[calc(100vh-128px)] number-fa">
            <div className="flex flex-col gap-6">
                {/* score */}
                <p className="text-xl font-medium sm:text-2xl text-dark-800 dark:text-dark-300 color-transition">
                    {t('SCORES')}: {score}
                </p>
                <p className="text-xl font-medium sm:text-2xl text-dark-800 dark:text-dark-300 color-transition">
                    {t('NUMBER_OF_LOOSES')}: {looseCount}
                </p>
            </div>
            {/* game */}
            <div className="flex justify-center col-span-3">
                <div className="max-w-[420px] xl:max-w-[440px] 2xl:max-w-[600px] w-full">
                    <div className="relative pt-[100%] w-full">
                        <div className="absolute top-0 left-0 w-full h-full">
                            <div className="grid w-full color-transition grid-cols-20 grid-rows-20">
                                {squares}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* timer */}
            <div className="text-xl font-medium sm:text-2xl text-dark-800 dark:text-dark-300 color-transition">
                {t('YOUR_TIME')}: {timer}
            </div>
        </section>
    );
}

export default Game;