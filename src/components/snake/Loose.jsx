import { useTranslation } from "react-i18next";

const Loose = ({score, timer, playAgain}) => {
    const {t} = useTranslation()
    return (
        <div className="flex-center gap-4 flex-col h-[calc(100vh-128px)] number-fa">
            <div className="h-96">
                <img src="../../public/img/snakeGame/gameOver.png" draggable={false} alt="Game Over" className="h-full" />
            </div>
            <p className="text-xl font-medium sm:text-2xl text-dark-800 dark:text-dark-300 color-transition">
                {t('YOUR_SCORE')}: {score}
            </p>
            <p className="text-xl font-medium sm:text-2xl text-dark-800 dark:text-dark-300 color-transition">
            {t('YOUR_TIME')}: {timer}
            </p>
            <div onClick={playAgain} className="h-10 text-base text-white bg-purple-500 rounded-md cursor-pointer w-28 dark:bg-purple-700 flex-center color-transition"> {t('PLAY_AGAIN')} </div>
        </div>
    );
}

export default Loose;