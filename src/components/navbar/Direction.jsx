import { useEffect } from "react";
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { FaDirection, EnDirection } from '../../stateManagement/Actions/index';

const Direction = (props) => {

    const { i18n } = useTranslation();

    const changeDirection = () => {
        if (props.direction) {
            document.dir = "rtl";
            props.FaDirection()
            i18n.changeLanguage('fa')
        } else {
            document.dir = "ltr";
            props.EnDirection()
            i18n.changeLanguage('en')
        }
        localStorage.setItem('lang', props.direction ? 'fa' : 'en')
    }

    function checklang() {
        if (localStorage.getItem('lang') === null) {
            i18n.changeLanguage('en')
            return false;
        } else if (localStorage.getItem('lang') === 'fa') {
            i18n.changeLanguage('fa')
            return false;
        } else if (localStorage.getItem('lang') === 'en') {
            i18n.changeLanguage('en')
            return false;
        }
    }

    useEffect(() => {
        checklang()
    }, [])

    return (
        <div className="block cursor-pointer">
            {props.direction ? (<img onClick={() => changeDirection("fa")} src="/img/flags/iran.png" alt="iran" className="rounded-full w-7 h-7" />) : (<img onClick={() => changeDirection("en")} src="/img/flags/usa.jpg" alt="iran" className="rounded-full w-7 h-7" />)}
        </div>
    );
}

let mapDirectionToProps = state => {
    return {
        direction: !state.direction
    }
}

export default connect(mapDirectionToProps, {
    FaDirection, EnDirection
})(Direction);