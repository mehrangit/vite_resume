import { useState } from "react";
import { connect } from "react-redux";
import { ToggleState } from "../../stateManagement/Actions/index";

const Darkmode = (props) => {

    const [dark, setDark] = useState(checkDarkMode())

    const handleDarkMode = () => {
        setDark(!dark)

        if (!props.state) {
            document.querySelector('body').classList.add('dark');
        } else {
            document.querySelector('body').classList.remove('dark');
        }
        localStorage.setItem('isDark', document.body.classList.contains('dark') ? 'true' : 'false')


        props.ToggleState()
    }

    function checkDarkMode() {
        if (localStorage.getItem('isDark') === null) {
            return false
        } else if (localStorage.getItem('isDark') === 'true') {
            document.body.classList.add('dark')
            return true;
        } else if (localStorage.getItem('isDark') === 'false') {
            document.body.classList.remove('dark')
            return false;
        }
    }

    return (
        <div className="block cursor-pointer">
            {props.state ? (<svg onClick={() => handleDarkMode()} xmlns="http://www.w3.org/2000/svg" className="w-7 h-7 stroke-yellow-400 fill-yellow-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                <path strokeLinecap="round" strokeLinejoin="round" d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>) : (<svg onClick={() => handleDarkMode()} xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 stroke-yellow-400 fill-yellow-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                <path strokeLinecap="round" strokeLinejoin="round" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" />
            </svg>)}
        </div>
    );
}

let mapStateToProps = state => {
    // console.log(state.darkmode);
    return {
        state: state.darkmode
    }
}

export default connect(mapStateToProps, { ToggleState })(Darkmode);