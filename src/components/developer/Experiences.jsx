import { useState } from "react";
import Experience from "./Experience";
import { useTranslation } from "react-i18next";

const Experiences = ({data}) => {

    const { t } = useTranslation();

    const [experienceQ, setExperienceQ] = useState(checkWorksamplesQuantity());

    function checkWorksamplesQuantity(){
        if(data.experiences.length >= 4){
            return true
        } else {
            return false
        }
    }

    return (
        <div className="py-6" id="worksamples">
            <p className="mb-4 text-sm font-medium sm:mb-6 sm:text-base text-dark-500 color-transition dark:text-dark-300"> {t('common:EXPRIENCES')} </p>
            <ul className={`flex flex-col gap-6 overflow-hidden`}>
                {data.experiences.map((experience, index) => (
                    <Experience experience={experience} index={index} key={index} toggle={experienceQ} />
                ))}
            </ul>
            {experienceQ ? (<div className="flex justify-center mt-6">
                <div onClick={() => setExperienceQ(false)} className="px-12 py-3 text-xs font-normal bg-gray-100 border border-transparent rounded-lg cursor-pointer color-transition dark:bg-dark-800 dark:text-dark-300 dark:border-dark-500 sm:rounded-xl sm:text-sm text-dark-400"> {t('common:SHOW_MORE')} </div>
            </div>) : ""}
        </div>
    );
}

export default Experiences;