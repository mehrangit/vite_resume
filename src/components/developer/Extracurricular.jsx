import { useState } from "react";
import { useTranslation } from "react-i18next";

const Extracurricular = ({data}) => {

    const { t } = useTranslation();

    const [course, setCourse] = useState(true);

    return (
        <div className="py-6" id="activities">
            <p className="mb-4 text-sm font-medium sm:mb-6 sm:text-base text-dark-500 color-transition dark:text-dark-300"> {t('common:EXTRACURRICULAR')} </p>
            <ul className={`flex flex-col gap-6 overflow-hidden ${course ? "max-h-60 sm:max-h-[21rem]" : "max-h-full"}`}>
                {data.courses.map((course, index) => (
                    <li key={index} className="flex items-center gap-4 sm:gap-6">
                        {/* logo */}
                        <div className="w-16 h-16 border rounded-full sm:w-24 sm:h-24 flex-center border-dark-250 color-transition dark:border-dark-500 dark:bg-dark-800">
                            <img src={course.src} title={t(course.name)} className="rounded-full w-14 h-14 sm:w-20 sm:h-20" />
                        </div>
                        {/* info */}
                        <div className="flex flex-col gap-1 text-xs font-medium sm:text-sm">
                            <a href={course.href} className="text-dark-500 color-transition dark:text-dark-300"> {t(course.name)} </a>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default Extracurricular;