import { useState } from "react";
import { useTranslation } from "react-i18next";

const Experience = ({experience, index, toggle}) => {

    const { t } = useTranslation();
    
    const [showDescription, setShowDescription] = useState(false)

    return (
        <li key={index} className={`${toggle && index < 3 ? "flex" : !toggle ? "flex" : "hidden"} flex-col`}>
            <div className="flex items-center justify-between">
                {/* image & names */}
                <div className="flex items-center gap-4 sm:gap-6">
                    {/* logo */}
                    <div className="flex-none w-16 h-16 border rounded-full sm:w-24 sm:h-24 flex-center border-dark-250 color-transition dark:border-dark-500 dark:bg-dark-800">
                        <img src={experience.src} title={t(experience.name)} className="w-10 h-10 sm:w-16 sm:h-16" />
                    </div>
                    {/* info */}
                    <div className="flex flex-col gap-1 text-xs font-medium sm:gap-2 sm:text-sm">
                        <a href={experience.href} className="cursor-pointer line-clamp-1 text-dark-500 hover:text-purple-500 dark:hover:text-purple-700 color-transition dark:text-dark-300"> {t(experience.name)} </a>
                        <p className="text-dark-300 color-transition dark:font-normal"> {t(experience.time)} </p>
                        <p className="text-purple-500 color-transition dark:text-purple-700"> {t(experience.task)} </p>
                    </div>
                </div>
                {/* more button */}
                {
                    !!experience.description && <div onClick={() => setShowDescription(!showDescription)} className="px-2 py-1 text-sm font-medium text-purple-500 cursor-pointer dark:text-purple-700"> {t('common:INFO')} </div>
                }
                
            </div>
            {/* description */}
            {!!experience.description && (
                <div className={`${showDescription ? "max-h-96" : "max-h-0"} overflow-hidden transition-all duration-500`}>
                <p className="pt-4 text-sm font-medium text-dark-900 dark:text-dark-300 color-transition">
                {t(experience.description)}
                </p>
            </div>
            )}
            
        </li>
    );
}

export default Experience;