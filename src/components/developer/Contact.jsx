import { useTranslation } from "react-i18next";
import { toast } from 'react-toastify';
const Contact = ({data}) => {

    const {t} = useTranslation();
    const notify = () => toast(t('UNDER_CONSTRUCTION'));
    
    
    return (
        <div className="flex flex-col gap-8 py-6 lg:flex-row lg:items-center" id="contact_us">
            <section className="flex flex-col lg:flex-1">
                <h2 className="mb-12 text-xl font-semibold sm:mb-8 text-dark-900 dark:text-dark-300"> {t('CONTACT_WITH_ME')} </h2>
                <div className="grid grid-cols-1 gap-8 mb-6 sm:gap-x-6 sm:grid-cols-2 lg:grid-cols-1">
                    {/* name input */}
                    <div className="relative w-full h-10 sm:h-12">
                        <input type="text" name="name" required className="w-full h-full border rounded-md bg-zinc-100 dark:bg-dark-800 dark:border-dark-500 dark:hover:border-zinc-600 dark:text-dark-300 border-zinc-300 hover:border-zinc-400 color-transition peer" />
                        <span className="text-sm font-medium text-green-500 absolute top-3 sm:top-3.5 left-4 rtl:left-auto rtl:right-4 z-[1] peer-focus:-top-6 peer-valid:-top-6 peer-valid:left-0 rtl:peer-valid:left-auto rtl:peer-valid:right-0 peer-valid:text-base peer-invalid:text-purple-500 dark:peer-invalid:text-purple-700 peer-focus:left-0 rtl:peer-focus:left-auto rtl:peer-focus:right-0 transition-all peer-focus:text-base"> {t('NAME')} </span>
                    </div>
                    {/* phone input */}
                    <div className="relative w-full h-10 sm:h-12">
                        <input type="tel" name="phone" required className="w-full h-full border rounded-md bg-zinc-100 dark:bg-dark-800 dark:border-dark-500 dark:hover:border-zinc-600 dark:text-dark-300 border-zinc-300 hover:border-zinc-400 color-transition peer" />
                        <span className="text-sm font-medium text-green-500 absolute top-3 sm:top-3.5 left-4 rtl:left-auto rtl:right-4 z-[1] peer-focus:-top-6 peer-valid:-top-6 peer-valid:left-0 rtl:peer-valid:left-auto rtl:peer-valid:right-0 peer-valid:text-base peer-invalid:text-purple-500 dark:peer-invalid:text-purple-700 peer-focus:left-0 rtl:peer-focus:left-auto rtl:peer-focus:right-0 transition-all peer-focus:text-base"> {t('PHONE_NUMBER')} </span>
                    </div>
                    {/* email input */}
                    <div className="relative w-full h-10 sm:h-12">
                        <input type="email" name="email" required className="w-full h-full border rounded-md bg-zinc-100 dark:bg-dark-800 dark:border-dark-500 dark:hover:border-zinc-600 dark:text-dark-300 border-zinc-300 hover:border-zinc-400 color-transition peer" />
                        <span className="text-sm font-medium text-green-500 absolute top-3 sm:top-3.5 left-4 rtl:left-auto rtl:right-4 z-[1] peer-focus:-top-6 peer-valid:-top-6 peer-valid:left-0 rtl:peer-valid:left-auto rtl:peer-valid:right-0 peer-valid:text-base peer-invalid:text-purple-500 dark:peer-invalid:text-purple-700 peer-focus:left-0 rtl:peer-focus:left-auto rtl:peer-focus:right-0 transition-all peer-focus:text-base"> {t('EMAIL')} </span>
                    </div>
                    {/* textarea */}
                    <div className="relative w-full h-32 col-span-full">
                        <textarea name="message" required className="w-full h-full border rounded-md bg-zinc-100 dark:bg-dark-800 dark:border-dark-500 dark:hover:border-zinc-600 dark:text-dark-300 border-zinc-300 hover:border-zinc-400 color-transition peer"></textarea>
                        <span className="text-sm font-medium text-green-500 absolute top-3 sm:top-3.5 left-4 rtl:left-auto rtl:right-4 z-[1] peer-focus:-top-6 peer-valid:-top-6 peer-valid:left-0 rtl:peer-valid:left-auto rtl:peer-valid:right-0 peer-valid:text-base peer-invalid:text-purple-500 dark:peer-invalid:text-purple-700 peer-focus:left-0 rtl:peer-focus:left-auto rtl:peer-focus:right-0 transition-all peer-focus:text-base"> {t('MESSAGE')} </span>
                    </div>
                </div>
                <div className="h-10 text-base text-white bg-purple-500 rounded-md cursor-pointer w-28 dark:bg-purple-700 flex-center color-transition" onClick={notify}> {t('SUBMIT')} </div>
            </section>
            {/* image */}
            <section className="flex justify-end lg:flex-1">
                <img src={data.avatar.contact} alt="contact image" className="max-h-[640px] rounded-md" />
            </section>
        </div>
    );
}

export default Contact;