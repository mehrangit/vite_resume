import { useState } from "react";
import { useTranslation } from "react-i18next";

const Skills = ({data}) => {

    const { t } = useTranslation();

    const [skill, setSkill] = useState(checkSkillQuantity());

    function checkSkillQuantity(){
        if(data.skill.length >= 4){
            return true
        } else {
            return false
        }
    }

    return (
        <div className="py-6" id="skills">
            <p className="mb-4 text-sm font-medium sm:mb-6 sm:text-base text-dark-500 color-transition dark:text-dark-300"> {t('common:SKILLS')} </p>
            <ul className={`flex flex-col gap-6 overflow-hidden ${skill ? "max-h-60 sm:max-h-[21rem]" : "max-h-full"}`}>
                {data.skill.map((skill, index) => (
                    <li key={index} className="flex items-center gap-4 sm:gap-6">
                        {/* logo */}
                        <div className="w-16 h-16 border rounded-full sm:w-24 sm:h-24 flex-center border-dark-250 color-transition dark:border-dark-500 dark:bg-dark-800">
                            <img src={skill.src} title={skill.name} className="w-10 h-10 sm:w-16 sm:h-16" />
                        </div>
                        {/* info */}
                        <div className="flex flex-col gap-1 text-xs font-medium sm:text-sm sm:gap-2">
                            <p className="text-dark-500 color-transition dark:text-dark-300"> {skill.name} </p>
                            <p className="text-dark-300 color-transition dark:text-dark-300 dark:font-normal"> {t(skill.time)} </p>
                        </div>
                    </li>
                ))}
            </ul>
            {skill ? (<div className="flex justify-center mt-6">
                <div onClick={() => setSkill(false)} className="px-12 py-3 text-xs font-normal bg-gray-100 border border-transparent rounded-lg cursor-pointer color-transition dark:text-dark-300 dark:border-dark-500 dark:bg-dark-800 sm:text-sm text-dark-400"> {t('common:SHOW_MORE')} </div>
            </div>) : ""}
        </div>
    );
}

export default Skills;