import { useTranslation } from "react-i18next";

const ShortDescription = ({ data }) => {

    const { t } = useTranslation();

    return (
        <div className="flex flex-col items-center gap-4 sm:gap-6">
            {/* image */}
            <div className="relative w-36 h-36">
                <img src={data.avatar.src} alt={data.name} className="w-full h-full rounded-full" />
                <img src={data.avatar.flag} alt={data.avatar.country} className="absolute w-10 h-10 sm:w-12 sm:h-12 border-[3px] border-white color-transition dark:border-dark-800 rounded-full bottom-1 right-1 rtl:right-auto rtl:left-1 sm:bottom-0 sm:right-0 rtl:sm:right-auto rtl:sm:left-0" />
            </div>
            {/* name */}
            <p className="text-xl font-medium sm:text-2xl text-dark-800 dark:text-dark-300 color-transition"> {data.name} </p>
            {/* short description */}
            <p className="text-sm font-normal leading-6 text-center sm:text-base sm:px-8 text-dark-400 color-transition dark:text-dark-300">
                {data.short_description}
            </p>
            {/* social media & projects */}
            <div className="flex items-center justify-between w-full lg:justify-center lg:gap-8">
                {/* projects */}
                <div className="flex items-center gap-1 select-none sm:gap-2">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5 sm:w-6 sm:h-6 stroke-dark-400 dark:stroke-dark-600 dark:fill-dark-300 color-transition lg:dark:w-7 lg:dark:h-7" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
                        </svg>
                    </div>
                    <p className="mt-1 text-sm font-normal sm:text-base text-dark-400 color-transition rtl:number-fa dark:text-dark-300"> {data.projects} {t('common:PROJECTS')} </p>
                </div>
                {/* social medias */}
                <div className="flex items-center gap-2 sm:gap-4">
                    {/* linkedin */}
                    <a href={data.social_media.linkedin} className="bg-white border border-transparent rounded-full shadow-2xl cursor-pointer group dark:shadow-none hover:border-purple-500 dark:hover:border-purple-700 color-transition dark:bg-dark-800 shadow-dark-800 w-7 h-7 sm:w-10 sm:h-10 flex-center dark:border-dark-550">
                        <svg className="w-4 h-4 sm:w-6 sm:h-6 fill-dark-400 dark:fill-dark-300 color-transition group-hover:fill-purple-500 dark:group-hover:fill-purple-700" fill="current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                            <path d="M9,25H4V10h5V25z M6.501,8C5.118,8,4,6.879,4,5.499S5.12,3,6.501,3C7.879,3,9,4.121,9,5.499C9,6.879,7.879,8,6.501,8z M27,25h-4.807v-7.3c0-1.741-0.033-3.98-2.499-3.98c-2.503,0-2.888,1.896-2.888,3.854V25H12V9.989h4.614v2.051h0.065 c0.642-1.18,2.211-2.424,4.551-2.424c4.87,0,5.77,3.109,5.77,7.151C27,16.767,27,25,27,25z" />
                        </svg>
                    </a>
                    {/* instagram */}
                    <a href={data.social_media.instagram} className="bg-white border border-transparent rounded-full shadow-2xl cursor-pointer group dark:shadow-none hover:border-purple-500 dark:hover:border-purple-700 color-transition dark:bg-dark-800 shadow-dark-800 w-7 h-7 sm:w-10 sm:h-10 flex-center dark:border-dark-550">
                        <svg className="w-4 h-4 sm:w-6 sm:h-6 fill-dark-400 dark:fill-dark-300 color-transition group-hover:fill-purple-500 dark:group-hover:fill-purple-700" fill="current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50">
                            <path d="M 16 3 C 8.83 3 3 8.83 3 16 L 3 34 C 3 41.17 8.83 47 16 47 L 34 47 C 41.17 47 47 41.17 47 34 L 47 16 C 47 8.83 41.17 3 34 3 L 16 3 z M 37 11 C 38.1 11 39 11.9 39 13 C 39 14.1 38.1 15 37 15 C 35.9 15 35 14.1 35 13 C 35 11.9 35.9 11 37 11 z M 25 14 C 31.07 14 36 18.93 36 25 C 36 31.07 31.07 36 25 36 C 18.93 36 14 31.07 14 25 C 14 18.93 18.93 14 25 14 z M 25 16 C 20.04 16 16 20.04 16 25 C 16 29.96 20.04 34 25 34 C 29.96 34 34 29.96 34 25 C 34 20.04 29.96 16 25 16 z" />
                        </svg>
                    </a>
                    {/* gmail */}
                    <a href={data.social_media.gmail} className="bg-white border border-transparent rounded-full shadow-2xl cursor-pointer group dark:shadow-none hover:border-purple-500 dark:hover:border-purple-700 color-transition dark:bg-dark-800 shadow-dark-800 w-7 h-7 sm:w-10 sm:h-10 flex-center dark:border-dark-550">
                        <svg className="w-4 h-4 sm:w-6 sm:h-6 fill-dark-400 dark:fill-dark-300 color-transition group-hover:fill-purple-500 dark:group-hover:fill-purple-700" fill="current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50">
                            <path d="M12 23.403V23.39 10.389L11.88 10.3h-.01L9.14 8.28C7.47 7.04 5.09 7.1 3.61 8.56 2.62 9.54 2 10.9 2 12.41v3.602L12 23.403zM38 23.39v.013l10-7.391V12.41c0-1.49-.6-2.85-1.58-3.83-1.46-1.457-3.765-1.628-5.424-.403L38.12 10.3 38 10.389V23.39zM14 24.868l10.406 7.692c.353.261.836.261 1.189 0L36 24.868V11.867L25 20l-11-8.133V24.868zM38 25.889V41c0 .552.448 1 1 1h6.5c1.381 0 2.5-1.119 2.5-2.5V18.497L38 25.889zM12 25.889L2 18.497V39.5C2 40.881 3.119 42 4.5 42H11c.552 0 1-.448 1-1V25.889z" />
                        </svg>
                    </a>
                    {/* phone */}
                    <a href={data.social_media.phone} className="bg-white border border-transparent rounded-full shadow-2xl cursor-pointer group dark:shadow-none hover:border-purple-500 dark:hover:border-purple-700 color-transition dark:bg-dark-800 shadow-dark-800 w-7 h-7 sm:w-10 sm:h-10 flex-center dark:border-dark-550">
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4 sm:w-6 sm:h-6 fill-dark-400 dark:fill-dark-300 color-transition group-hover:fill-purple-500 dark:group-hover:fill-purple-700" viewBox="0 0 20 20" fill="currentColor">
                            <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    );
}

export default ShortDescription;