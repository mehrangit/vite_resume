import { useTranslation } from "react-i18next";

const Product = ({course, fillCartTransitionAction, filter}) => {
    const {t} = useTranslation()
    return (
        <div className={`relative ${filter ? 'flex' : 'hidden'} flex-col group overflow-hidden hover:shadow-lg transition-all duration-500 w-80 h-[21rem] rounded`}>
            <div className="w-full h-64">
                <img src={course.src} alt="elemen" className="w-full h-full" />
            </div>
            {/* overlay */}
            <div className="absolute top-0 left-0 w-full h-full z-[1] bg-teal-600/50 opacity-0 group-hover:opacity-100 transition-all duration-500"></div>
            {/* add to cart */}
            <div className="absolute flex justify-center py-1.5 text-base z-[2] font-medium text-white transition-all duration-500 -translate-x-1/2 border-2 border-white rounded-md opacity-0 cursor-pointer group-hover:opacity-100 w-40 top-8 left-1/2" onClick={(e) => fillCartTransitionAction(e, course.id)}> {t('cart:ADDTOCART')} </div>
            {/* view */}
            <a href="#" className="absolute flex justify-center py-1.5 z-[2] text-base font-medium text-white transition-all duration-500 -translate-x-1/2 border-2 border-white rounded-md opacity-0 cursor-pointer group-hover:opacity-100 w-40 top-24 left-1/2"> {t('cart:MOREINFO')} </a>
            {/* info */}
            <div className="absolute left-0 w-full p-4 transition-all z-[2] duration-500 group-hover:bottom-0 -bottom-20 bg-dark-100 dark:bg-dark-600">
                <p className="mb-2 text-lg font-medium title-color color-transition line-clamp-1"> {t(course.name)} </p>
                <p className="mb-6 text-sm font-normal text-green-500 color-transition"> {course.price} {t(course.unit)} </p>
                <div className="flex flex-col gap-1">
                    <p className="text-base font-medium title-color color-transition"> {t('PREREQUISITE')} </p>
                    <p className="text-sm italic font-normal text-gray-400 color-transition"> {course.pre} </p>
                </div>
            </div>
        </div>
    );
}

export default Product;