/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: 'class',
  theme: {
    screens: {
      'xs': '440px',
      ...defaultTheme.screens,
  },
  container: {
      center: true,
      padding: '1rem',
  },
  extend: {
      aspectRatio: {
          '4/3': '4 / 3',
      },
      backgroundImage: {
          'mehran': "url('../public/img/profiles/mehran.jpg')",
      },
      borderRadius: {
          '4xl': '32px',
      },
      colors: {
          'dark': {
              100: '#f2f2f2',
              200: '#eeeeee',
              250: '#e6e6e6',
              300: '#aaaab3',
              400: '#747683',
              500: '#404046',
              550: '#3f3f45',
              600: '#3b3b41',
              700: '#323236',
              800: '#28282b',
              900: '#232326',
          },
          'purple': {
              500: '#671cc9',
              700: '#797bf2',
          }
      },
      gridTemplateColumns: {
        // Simple 20 column grid
        '20': 'repeat(20, minmax(0, 1fr))',
      },
      gridTemplateRows: {
        // Simple 20 row grid
        '20': 'repeat(20, minmax(0, 1fr))',
      },
      minHeight: {
          '16': 'calc(100vh - 64px)',
      },
      spacing: {
          '1/8': '12.5%',
          '2/8': '25%',
          '3/8': '37.5%',
          '4/8': '50%',
          '5/8': '62.5%',
          '6/8': '75%',
          '7/8': '87.5%',
      }
  },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('tailwind-scrollbar')({ nocompatible: true }),
    require('@tailwindcss/aspect-ratio'),
  ],
}

